# GHC/head.hackage CI support
# ===========================
#
# This is the GitLab CI automation that drives GHC's head.hackage testing.
# The goal is to be able to test GHC by building a (small) subset of Hackage.
# Moreover, we want to be able to collect logs of failed builds as well as
# performance metrics from builds that succeed.
#
# To accomplish this we use head.hackage's native Nix support and the
# ghc-artefact-nix expression to make GHC binary distributions usable from
# within Nix. These components are tied together by ./scripts/build-all.nix,
# which contains the list of packages which we build as well as some simple
# configuration to minimize the cost of the builds.
#

stages:
  - test
  - update-repo
  - deploy

variables:
  # Which nixos/nix Docker image tag to use
  DOCKER_TAG: "2.3"

  # Default GHC bindist
  GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/master/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"

  # Project ID of ghc/ghc
  GHC_PROJECT_ID: "1"

  # ACCESS_TOKEN provided via protected environment variable

  # EXTRA_HC_OPTS provided by GHC job. These are passed to via --ghc-options to
  # GHC during the package builds. This is instantiated with, e.g., -dcore-lint
  # during GHC validation builds.

# A build triggered from a ghc/ghc> pipeline.
build-pipeline:
  extends: .build
  before_script:
    - |
      if [ -n "$GHC_PIPELINE_ID" ]; then
        job_name="validate-x86_64-linux-fedora27"
        job_id=$(nix run -f ci/default.nix \
          -c find-job $GHC_PROJECT_ID $GHC_PIPELINE_ID $job_name)
        echo "Pulling ${job_name} binary distribution from Pipeline $GHC_PIPELINE_ID (job $job_id)..."
      fi
  rules:
    - if: '$GHC_PIPELINE_ID'

# Build against the master branch
build-master:
  extends: .build
  variables:
    GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/master/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"
    EXTRA_HC_OPTS: "-dcore-lint -ddump-timings"
  rules:
    - if: '$GHC_PIPELINE_ID != ""'

# Build against the 9.0 branch
build-9.0:
  extends: .build
  variables:
    GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/ghc-9.0/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"
    EXTRA_HC_OPTS: "-dcore-lint"
  rules:
    - if: '$GHC_PIPELINE_ID != ""'

.build:
  stage: test

  tags:
    - x86_64-linux

  image: "nixos/nix:$DOCKER_TAG"

  cache:
    key: build-HEAD
    paths:
      - store.nar

  before_script:
    - |
      if [ -e store.nar ]; then
        echo "Extracting cached Nix store..."
        nix-store --import -vv < store.nar || echo "invalid cache"
      else
        echo "No cache found"
      fi

  script:
      # Install GHC
    - echo "Bindist tarball is $GHC_TARBALL"
    - |
      nix build \
      -f https://github.com/mpickering/ghc-artefact-nix/archive/master.tar.gz \
      --argstr url $GHC_TARBALL \
      --out-link ghc \
      ghcHEAD
    - export GHC=`pwd`/ghc/bin/ghc
    - rm -Rf $HOME/.cabal/packages/local ci/run
      # Build CI executable
    - |
      nix-build ./ci -j$CPUS --no-build-output
      nix-store --export \
        $(nix-store -qR --include-outputs \
          $(nix-instantiate --quiet ./ci)) \
      > store.nar
      # Test it
    - nix run -f ./ci -c run-ci

  after_script:
    - ls -lh
    - |
      nix run -f ./ci -c \
      tar -cJf results.tar.xz -C ci/run \
      results.json logs compiler-info

  artifacts:
    when: always
    paths:
    - results.tar.xz

# Build and deploy a Hackage repository
update-repo:
  stage: update-repo

  tags:
    - x86_64-linux
    - head.hackage

  image: "nixos/nix:$DOCKER_TAG"

  variables:
    KEYS_TARBALL: https://downloads.haskell.org/ghc/head.hackage-keys.tar.enc
    # KEYS_TARBALL_KEY provided by protected variable

  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

  script:
    - nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
    - nix-channel --update
    - nix build -f ci/default.nix
    - nix run -f ci/default.nix -c build-repo.sh extract-keys
    - nix run -f ci/default.nix -c build-repo.sh build-repo

  dependencies:
    - build-master

  after_script:
    - rm -Rf keys

  artifacts:
    paths:
      - repo

pages:
  stage: deploy
  tags:
    - x86_64-linux
    - head.hackage
  image: "nixos/nix:$DOCKER_TAG"
  script:
    - mv repo public
  dependencies:
    - update-repo
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  artifacts:
    paths:
      - public
