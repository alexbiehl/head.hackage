{ nixpkgs ? (import (import ./base-nixpkgs.nix) {}) }:

with nixpkgs;
let
  hackage-repo-tool =
    let
      src = fetchFromGitHub {
        owner = "haskell";
        repo = "hackage-security";
        rev = "474768743f407edef988d4153f081b2c662fe84f";
        sha256 = "1n46ablyna72zl9whirfv72l715wlmi6jahbf02asbsxkx3b7xnm";
      };
    in haskellPackages.callCabal2nix "hackage-repo-tool" "${src}/hackage-repo-tool" {};

  overlay-tool =
    let
      src = fetchFromGitHub {
        owner = "bgamari";
        repo = "hackage-overlay-repo-tool";
        rev = "7aac81e9bc468b103dd78b9c662672c86fe236f7";
        sha256 = "0i4iw8nbhvc2xx05c0hbnnjyhap3b4xsclmxnmfa6dsa2ym02jc0";
      };
    in haskellPackages.callCabal2nix "hackage-overlay-repo-tool" src {};

  head-hackage-ci =
    let
      src = nixpkgs.nix-gitignore.gitignoreSource [] ./.;
    in haskellPackages.callCabal2nix "head-hackage-ci" src {};

  buildDepsFragment =
    let
      buildDeps = import ./build-deps.nix { pkgs = nixpkgs; };

      mkCabalFragment = pkgName: deps:
        with pkgs.lib;
        let
          libDirs = concatStringsSep " " (map (dep: getOutput "lib" dep + "/lib") deps);
          includeDirs = concatStringsSep " " (map (dep: getOutput "dev" dep + "/include") deps);
        in ''
        package ${pkgName}
          extra-lib-dirs: ${libDirs}
          extra-include-dirs: ${includeDirs}
        '';
    in
      pkgs.lib.concatStringsSep "\n"
        (pkgs.lib.mapAttrsToList mkCabalFragment buildDeps);

  buildDepsFile = pkgs.writeText "deps.cabal.project" buildDepsFragment;

  build-repo =
    let
      deps = [
        bash curl gnutar findutils patch rsync openssl
        cabal-install ghc gcc binutils-unwrapped pwgen gnused
        hackage-repo-tool overlay-tool python3 jq
        git # cabal-install wants this to fetch source-repository-packages
      ];
    in
      runCommand "repo" {
        nativeBuildInputs = [ makeWrapper ];
        cabalDepsSrc = buildDepsFragment;
      } ''
        mkdir -p $out/bin
        makeWrapper ${head-hackage-ci}/bin/head-hackage-ci $out/bin/head-hackage-ci \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${./build-repo.sh} $out/bin/build-repo.sh \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${../run-ci} $out/bin/run-ci \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin \
            --set USE_NIX 1 \
            --set CI_CONFIG ${./config.sh}

        makeWrapper ${./find-job.sh} $out/bin/find-job \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${xz}/bin/xz $out/bin/xz
      '';
in
  build-repo
