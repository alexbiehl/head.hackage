diff --git a/Crypto/Internal/WordArray.hs b/Crypto/Internal/WordArray.hs
index 0f3c0f6..f37a2fd 100644
--- a/Crypto/Internal/WordArray.hs
+++ b/Crypto/Internal/WordArray.hs
@@ -10,6 +10,7 @@
 --
 -- The array produced should never be exposed to the user directly.
 --
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE BangPatterns #-}
 {-# LANGUAGE MagicHash #-}
 {-# LANGUAGE UnboxedTuples #-}
@@ -102,7 +103,7 @@ mutableArray32 (I# n) l = IO $ \s ->
         loop i st mb ((W32# x):xs)
             | booleanPrim (i ==# n) = (# st, MutableArray32 mb #)
             | otherwise =
-                let !st' = writeWord32Array# mb i x st
+                let !st' = writeWord32Array# mb i (extendWord32Compat# x) st
                  in loop (i +# 1#) st' mb xs
 
 -- | Create a Mutable Array of BE Word32 aliasing an Addr
@@ -125,12 +126,12 @@ mutableArray32Freeze (MutableArray32 mb) = IO $ \st ->
 
 -- | Read a Word8 from an Array
 arrayRead8 :: Array8 -> Int -> Word8
-arrayRead8 (Array8 a) (I# o) = W8# (indexWord8OffAddr# a o)
+arrayRead8 (Array8 a) (I# o) = W8# (narrowWord8Compat# (indexWord8OffAddr# a o))
 {-# INLINE arrayRead8 #-}
 
 -- | Read a Word32 from an Array
 arrayRead32 :: Array32 -> Int -> Word32
-arrayRead32 (Array32 b) (I# o) = W32# (indexWord32Array# b o)
+arrayRead32 (Array32 b) (I# o) = W32# (narrowWord32Compat# (indexWord32Array# b o))
 {-# INLINE arrayRead32 #-}
 
 -- | Read a Word64 from an Array
@@ -140,12 +141,12 @@ arrayRead64 (Array64 b) (I# o) = W64# (indexWord64Array# b o)
 
 -- | Read a Word32 from a Mutable Array of Word32
 mutableArrayRead32 :: MutableArray32 -> Int -> IO Word32
-mutableArrayRead32 (MutableArray32 m) (I# o) = IO $ \s -> case readWord32Array# m o s of (# s', e #) -> (# s', W32# e #)
+mutableArrayRead32 (MutableArray32 m) (I# o) = IO $ \s -> case readWord32Array# m o s of (# s', e #) -> (# s', W32# (narrowWord32Compat# e) #)
 {-# INLINE mutableArrayRead32 #-}
 
 -- | Write a Word32 from a Mutable Array of Word32
 mutableArrayWrite32 :: MutableArray32 -> Int -> Word32 -> IO ()
-mutableArrayWrite32 (MutableArray32 m) (I# o) (W32# w) = IO $ \s -> let !s' = writeWord32Array# m o w s in (# s', () #)
+mutableArrayWrite32 (MutableArray32 m) (I# o) (W32# w) = IO $ \s -> let !s' = writeWord32Array# m o (extendWord32Compat# w) s in (# s', () #)
 {-# INLINE mutableArrayWrite32 #-}
 
 -- | Write into the Mutable Array of Word32 by combining through xor the current value and the new value.
@@ -155,3 +156,23 @@ mutableArrayWriteXor32 :: MutableArray32 -> Int -> Word32 -> IO ()
 mutableArrayWriteXor32 m o w =
     mutableArrayRead32 m o >>= \wOld -> mutableArrayWrite32 m o (wOld `xor` w)
 {-# INLINE mutableArrayWriteXor32 #-}
+
+#if MIN_VERSION_base(4,16,0)
+extendWord32Compat# :: Word32# -> Word#
+extendWord32Compat# = extendWord32#
+
+narrowWord8Compat# :: Word# -> Word8#
+narrowWord8Compat# = narrowWord8#
+
+narrowWord32Compat# :: Word# -> Word32#
+narrowWord32Compat# = narrowWord32#
+#else
+extendWord32Compat# :: Word# -> Word#
+extendWord32Compat# x = x
+
+narrowWord8Compat# :: Word# -> Word#
+narrowWord8Compat# x = x
+
+narrowWord32Compat# :: Word# -> Word#
+narrowWord32Compat# x = x
+#endif
diff --git a/Crypto/Number/Basic.hs b/Crypto/Number/Basic.hs
index e624b42..253b222 100644
--- a/Crypto/Number/Basic.hs
+++ b/Crypto/Number/Basic.hs
@@ -100,6 +100,7 @@ numBits n = gmpSizeInBits n `onGmpUnsupported` (if n == 0 then 1 else computeBit
 
 -- | Compute the number of bytes for an integer
 numBytes :: Integer -> Int
+numBytes 0 = 1
 numBytes n = gmpSizeInBytes n `onGmpUnsupported` ((numBits n + 7) `div` 8)
 
 -- | Express an integer as an odd number and a power of 2
diff --git a/Crypto/Number/Compat.hs b/Crypto/Number/Compat.hs
index 01e0455..4ebb9cb 100644
--- a/Crypto/Number/Compat.hs
+++ b/Crypto/Number/Compat.hs
@@ -51,7 +51,9 @@ onGmpUnsupported GmpUnsupported   f = f
 
 -- | Compute the GCDE of a two integer through GMP
 gmpGcde :: Integer -> Integer -> GmpSupported (Integer, Integer, Integer)
-#if MIN_VERSION_integer_gmp(0,5,1)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpGcde _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(0,5,1)
 gmpGcde a b =
     GmpSupported (s, t, g)
   where (# g, s #) = gcdExtInteger a b
@@ -72,7 +74,9 @@ gmpLog2 _ = GmpUnsupported
 -- | Compute the power modulus using extra security to remain constant
 -- time wise through GMP
 gmpPowModSecInteger :: Integer -> Integer -> Integer -> GmpSupported Integer
-#if MIN_VERSION_integer_gmp(1,0,2)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpPowModSecInteger _ _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(1,0,2)
 gmpPowModSecInteger b e m = GmpSupported (powModSecInteger b e m)
 #elif MIN_VERSION_integer_gmp(1,0,0)
 gmpPowModSecInteger _ _ _ = GmpUnsupported
@@ -84,7 +88,9 @@ gmpPowModSecInteger _ _ _ = GmpUnsupported
 
 -- | Compute the power modulus through GMP
 gmpPowModInteger :: Integer -> Integer -> Integer -> GmpSupported Integer
-#if MIN_VERSION_integer_gmp(0,5,1)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpPowModInteger _ _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(0,5,1)
 gmpPowModInteger b e m = GmpSupported (powModInteger b e m)
 #else
 gmpPowModInteger _ _ _ = GmpUnsupported
@@ -92,7 +98,9 @@ gmpPowModInteger _ _ _ = GmpUnsupported
 
 -- | Inverse modulus of a number through GMP
 gmpInverse :: Integer -> Integer -> GmpSupported (Maybe Integer)
-#if MIN_VERSION_integer_gmp(0,5,1)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpInverse _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(0,5,1)
 gmpInverse g m
     | r == 0    = GmpSupported Nothing
     | otherwise = GmpSupported (Just r)
@@ -103,7 +111,9 @@ gmpInverse _ _ = GmpUnsupported
 
 -- | Get the next prime from a specific value through GMP
 gmpNextPrime :: Integer -> GmpSupported Integer
-#if MIN_VERSION_integer_gmp(0,5,1)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpNextPrime _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(0,5,1)
 gmpNextPrime n = GmpSupported (nextPrimeInteger n)
 #else
 gmpNextPrime _ = GmpUnsupported
@@ -111,7 +121,9 @@ gmpNextPrime _ = GmpUnsupported
 
 -- | Test if a number is prime using Miller Rabin
 gmpTestPrimeMillerRabin :: Int -> Integer -> GmpSupported Bool
-#if MIN_VERSION_integer_gmp(0,5,1)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpTestPrimeMillerRabin _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(0,5,1)
 gmpTestPrimeMillerRabin (I# tries) !n = GmpSupported $
     case testPrimeInteger n tries of
         0# -> False
@@ -138,7 +150,9 @@ gmpSizeInBits _ = GmpUnsupported
 
 -- | Export an integer to a memory (big-endian)
 gmpExportInteger :: Integer -> Ptr Word8 -> GmpSupported (IO ())
-#if MIN_VERSION_integer_gmp(1,0,0)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpExportInteger _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(1,0,0)
 gmpExportInteger n (Ptr addr) = GmpSupported $ do
     _ <- exportIntegerToAddr n addr 1#
     return ()
@@ -152,7 +166,9 @@ gmpExportInteger _ _ = GmpUnsupported
 
 -- | Export an integer to a memory (little-endian)
 gmpExportIntegerLE :: Integer -> Ptr Word8 -> GmpSupported (IO ())
-#if MIN_VERSION_integer_gmp(1,0,0)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpExportIntegerLE _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(1,0,0)
 gmpExportIntegerLE n (Ptr addr) = GmpSupported $ do
     _ <- exportIntegerToAddr n addr 0#
     return ()
@@ -166,7 +182,9 @@ gmpExportIntegerLE _ _ = GmpUnsupported
 
 -- | Import an integer from a memory (big-endian)
 gmpImportInteger :: Int -> Ptr Word8 -> GmpSupported (IO Integer)
-#if MIN_VERSION_integer_gmp(1,0,0)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpImportInteger _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(1,0,0)
 gmpImportInteger (I# n) (Ptr addr) = GmpSupported $
     importIntegerFromAddr addr (int2Word# n) 1#
 #elif MIN_VERSION_integer_gmp(0,5,1)
@@ -178,7 +196,9 @@ gmpImportInteger _ _ = GmpUnsupported
 
 -- | Import an integer from a memory (little-endian)
 gmpImportIntegerLE :: Int -> Ptr Word8 -> GmpSupported (IO Integer)
-#if MIN_VERSION_integer_gmp(1,0,0)
+#if MIN_VERSION_integer_gmp(1,1,0)
+gmpImportIntegerLE _ _ = GmpUnsupported
+#elif MIN_VERSION_integer_gmp(1,0,0)
 gmpImportIntegerLE (I# n) (Ptr addr) = GmpSupported $
     importIntegerFromAddr addr (int2Word# n) 0#
 #elif MIN_VERSION_integer_gmp(0,5,1)
