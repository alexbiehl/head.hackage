diff --git a/src/Language/Haskell/Meta/Syntax/Translate.hs b/src/Language/Haskell/Meta/Syntax/Translate.hs
index 946f96d..9bc29c1 100644
--- a/src/Language/Haskell/Meta/Syntax/Translate.hs
+++ b/src/Language/Haskell/Meta/Syntax/Translate.hs
@@ -20,6 +20,7 @@ import qualified Data.Char                    as Char
 import qualified Data.List                    as List
 import qualified Language.Haskell.Exts.SrcLoc as Exts.SrcLoc
 import qualified Language.Haskell.Exts.Syntax as Exts
+import qualified Language.Haskell.TH          as TH
 import qualified Language.Haskell.TH.Syntax   as TH
 
 -----------------------------------------------------------------------------
@@ -37,7 +38,7 @@ class ToStmt a where toStmt :: a -> TH.Stmt
 class ToLoc  a where toLoc  :: a -> TH.Loc
 class ToCxt  a where toCxt  :: a -> TH.Cxt
 class ToPred a where toPred :: a -> TH.Pred
-class ToTyVars a where toTyVars :: a -> [TH.TyVarBndr]
+class ToTyVars a where toTyVars :: a -> [TyVarBndrUnit]
 class ToMaybeKind a where toMaybeKind :: a -> Maybe TH.Kind
 #if MIN_VERSION_template_haskell(2,11,0)
 class ToInjectivityAnn a where toInjectivityAnn :: a -> TH.InjectivityAnn
@@ -216,7 +217,12 @@ instance ToPat (Exts.Pat l) where
     TH.DoublePrimL r'' -> TH.DoublePrimL (negate r'')
     _                  -> nonsense "toPat" "negating wrong kind of literal" l
   toPat (Exts.PInfixApp _ p n q) = TH.UInfixP (toPat p) (toName n) (toPat q)
-  toPat (Exts.PApp _ n ps) = TH.ConP (toName n) (fmap toPat ps)
+  toPat (Exts.PApp _ n ps) = TH.ConP (toName n)
+-- TODO: Use MIN_VERSION_template_haskell(2,18,0) here (see https://gitlab.haskell.org/ghc/ghc/-/issues/19083)
+#if __GLASGOW_HASKELL__ >= 901
+                                     []
+#endif
+                                     (fmap toPat ps)
   toPat (Exts.PTuple _ Exts.Boxed ps) = TH.TupP (fmap toPat ps)
   toPat (Exts.PTuple _ Exts.Unboxed ps) = TH.UnboxedTupP (fmap toPat ps)
   toPat (Exts.PList _ ps) = TH.ListP (fmap toPat ps)
@@ -279,7 +285,11 @@ instance ToExp (Exts.Exp l) where
   toExp (Exts.If _ a b c)              = TH.CondE (toExp a) (toExp b) (toExp c)
   toExp (Exts.MultiIf _ ifs)           = TH.MultiIfE (map toGuard ifs)
   toExp (Exts.Case _ e alts)           = TH.CaseE (toExp e) (map toMatch alts)
-  toExp (Exts.Do _ ss)                 = TH.DoE (map toStmt ss)
+  toExp (Exts.Do _ ss)                 = TH.DoE
+#if MIN_VERSION_template_haskell(2,17,0)
+                                                Nothing
+#endif
+                                                (map toStmt ss)
   toExp e@Exts.MDo{}                   = noTH "toExp" e
   toExp (Exts.Tuple _ Exts.Boxed xs)   = TH.TupE (fmap toTupEl xs)
   toExp (Exts.Tuple _ Exts.Unboxed xs) = TH.UnboxedTupE (fmap toTupEl xs)
@@ -347,9 +357,15 @@ instance ToName (Exts.TyVarBind l) where
 instance ToName TH.Name where
   toName = id
 
+#if MIN_VERSION_template_haskell(2,17,0)
+instance ToName (TH.TyVarBndr flag) where
+  toName (TH.PlainTV n _)    = n
+  toName (TH.KindedTV n _ _) = n
+#else
 instance ToName TH.TyVarBndr where
   toName (TH.PlainTV n)    = n
   toName (TH.KindedTV n _) = n
+#endif
 
 #if !MIN_VERSION_haskell_src_exts(1,21,0)
 instance ToType (Exts.Kind l) where
@@ -375,12 +391,12 @@ instance ToType (Exts.Kind l) where
 toKind :: Exts.Kind l -> TH.Kind
 toKind = toType
 
-toTyVar :: Exts.TyVarBind l -> TH.TyVarBndr
-toTyVar (Exts.KindedVar _ n k) = TH.KindedTV (toName n) (toKind k)
-toTyVar (Exts.UnkindedVar _ n) = TH.PlainTV (toName n)
+toTyVar :: Exts.TyVarBind l -> TyVarBndrUnit
+toTyVar (Exts.KindedVar _ n k) = TH.kindedTV (toName n) (toKind k)
+toTyVar (Exts.UnkindedVar _ n) = TH.plainTV (toName n)
 
 instance ToType (Exts.Type l) where
-  toType (Exts.TyForall _ tvbM cxt t) = TH.ForallT (maybe [] (fmap toTyVar) tvbM) (toCxt cxt) (toType t)
+  toType (Exts.TyForall _ tvbM cxt t) = TH.ForallT (changeSpecs specifiedSpec (maybe [] (fmap toTyVar) tvbM)) (toCxt cxt) (toType t)
   toType (Exts.TyFun _ a b) = toType a .->. toType b
   toType (Exts.TyList _ t) = TH.ListT `TH.AppT` toType t
   toType (Exts.TyTuple _ b ts) = foldAppT (tuple . length $ ts) (fmap toType ts)
@@ -713,7 +729,7 @@ instance ToType (Exts.InstHead l) where
 
 qualConDeclToCon :: Exts.QualConDecl l -> TH.Con
 qualConDeclToCon (Exts.QualConDecl _ Nothing Nothing cdecl) = conDeclToCon cdecl
-qualConDeclToCon (Exts.QualConDecl _ ns cxt cdecl) = TH.ForallC (toTyVars ns)
+qualConDeclToCon (Exts.QualConDecl _ ns cxt cdecl) = TH.ForallC (changeSpecs specifiedSpec (toTyVars ns))
                                                     (toCxt cxt)
                                                     (conDeclToCon cdecl)
 
@@ -841,3 +857,36 @@ instance ToDecs a => ToDecs [a] where
   toDecs a = concatMap toDecs a
 
 -----------------------------------------------------------------------------
+
+#if MIN_VERSION_template_haskell(2,17,0)
+type TyVarBndr_ spec = TH.TyVarBndr spec
+type TyVarBndrSpec   = TH.TyVarBndrSpec
+type TyVarBndrUnit   = TH.TyVarBndrUnit
+
+specifiedSpec :: TH.Specificity
+specifiedSpec = TH.specifiedSpec
+
+plainTV' :: TH.Name -> TH.Specificity -> TyVarBndrSpec
+plainTV' = TH.PlainTV
+#else
+type TyVarBndr_ spec = TH.TyVarBndr
+type TyVarBndrSpec   = TH.TyVarBndr
+type TyVarBndrUnit   = TH.TyVarBndr
+
+specifiedSpec :: Specificity
+specifiedSpec = SpecifiedSpec
+
+plainTV' :: TH.Name -> Specificity -> TyVarBndrSpec
+plainTV' n _ = TH.PlainTV n
+
+data Specificity
+  = SpecifiedSpec
+  | InferredSpec
+#endif
+
+changeSpecs :: newSpec -> [TyVarBndr_ oldSpec] -> [TyVarBndr_ newSpec]
+#if MIN_VERSION_template_haskell(2,17,0)
+changeSpecs newSpec = map (newSpec <$)
+#else
+changeSpecs _ = id
+#endif
diff --git a/src/Language/Haskell/Meta/Utils.hs b/src/Language/Haskell/Meta/Utils.hs
index b434972..dcec37e 100644
--- a/src/Language/Haskell/Meta/Utils.hs
+++ b/src/Language/Haskell/Meta/Utils.hs
@@ -18,7 +18,7 @@ import Control.Monad
 import Data.Generics                hiding (Fixity)
 import Data.List                    (findIndex)
 import Language.Haskell.Exts.Pretty (prettyPrint)
-import Language.Haskell.Meta
+import Language.Haskell.Meta as Meta
 import Language.Haskell.TH.Lib      hiding (cxt)
 import Language.Haskell.TH.Ppr
 import Language.Haskell.TH.Syntax
@@ -161,7 +161,7 @@ renameT env new (ForallT ns cxt t) =
         (t',env4,new4) = renameT env3 new3 t
     in (ForallT ns'' cxt' t', env4, new4)
   where
-    unVarT (VarT n) = PlainTV n
+    unVarT (VarT n) = Meta.plainTV' n Meta.specifiedSpec
     unVarT ty       = error $ "renameT: unVarT: TODO for" ++ show ty
     renamePreds = renameThings renamePred
     renamePred = renameT
@@ -230,7 +230,7 @@ decCons (NewtypeD _ _ _ con _)   = [con]
 decCons _                        = []
 
 
-decTyVars :: Dec -> [TyVarBndr]
+decTyVars :: Dec -> [Meta.TyVarBndrUnit]
 #if MIN_VERSION_template_haskell(2,11,0)
 decTyVars (DataD _ _ ns _ _ _)    = ns
 decTyVars (NewtypeD _ _ ns _ _ _) = ns
@@ -329,7 +329,12 @@ fromDataConI (DataConI dConN ty _tyConN) =
   let n = arityT ty
   in replicateM n (newName "a")
       >>= \ns -> return (Just (LamE
-                    [ConP dConN (fmap VarP ns)]
+                    [ConP dConN
+-- TODO: Use MIN_VERSION_template_haskell(2,18,0) here (see https://gitlab.haskell.org/ghc/ghc/-/issues/19083)
+#if __GLASGOW_HASKELL__ >= 901
+                          []
+#endif
+                          (fmap VarP ns)]
 #if MIN_VERSION_template_haskell(2,16,0)
                     (TupE $ fmap (Just . VarE) ns)
 #else
